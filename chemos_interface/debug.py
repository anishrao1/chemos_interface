import os
import pickle
import petname
from .config import chemos_status_watch, CHEMOSPATH, INPUT_FOLDER

__doc__ = "Tools for debugging and offline testing"


def set_chemos_status(status):
    """
    Sets the chemos status to that specified

    :param status: desired status
    :return: previous status
    """
    with open(chemos_status_watch.contents[0], 'rb') as file:
        previous = pickle.load(file)
    with open(chemos_status_watch.contents[0], 'wb') as file:
        pickle.dump(
            {'status': status},
            file
        )
    return previous


def write_dummy_parameter_file(
        parameters,
        filename=None,
):
    """
    Writes parameters to a dummy parameters file.

    :param dict parameters: dictionary of parameters to write to file
    :param filename: optional filename. If none is specified, a random petname is used.
    """
    if filename is None:  # if not specified, generate file name
        filename = petname.generate(letters=10)
    if 'experiment_id' not in parameters:  # add experiment ID to parameters if not present
        parameters['experiment_id'] = filename
    fullpath = os.path.join(CHEMOSPATH, INPUT_FOLDER, f'{filename}.pkl')
    if os.path.isfile(fullpath) is True:
        if input(
                f'The file {filename} already exists in the target directory, overwrite? Y/N'
        ).lower() not in ['y', 'yes']:
            print('User declined to continue, writing aborted')
            return

    with open(fullpath, 'wb') as outputfile:
        pickle.dump(
            parameters,
            outputfile,
        )


def write_these_dummy_files(
        *experiments: dict,
):
    """
    Writes the provided dictionaries to dummy files.

    :param experiments: experiment dictionaries to write to dummy files
    """
    for exp in experiments:
        write_dummy_parameter_file(
            parameters=exp
        )
