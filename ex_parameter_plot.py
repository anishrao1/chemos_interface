"""
An example script to show how to use the parameter plot visualization.

The parameter_plot generates a figure with one subplot for each plot key provided versus a response value.
This plot can be helpful for visualizing the distribution of values in relation to some response factor.
"""

import os
import pylab as pl
from chemos_interface.visualization import parameter_plot
from chemos_interface.io import load_and_sort

# path to example files
examples_path = os.path.dirname(os.path.abspath(__file__))
folder = os.path.join(
    examples_path,
    'example_results',
)

# load pickle files and sort the experiments by timestamp
sorted_values = load_and_sort(folder)

# define the plot keys (each key yields one subplot)
plot_keys = [
    'Pd_mol%',
    'Rxn_temp',
    'P/Pd_ratio',
    'ArBA_equiv',
    'P_ligand',
]

# generate the parameter plot
fig, ax = parameter_plot(
    sorted_values,
    'E-PR AY',
    *plot_keys,
)
# the figure and axes objects can be caught and further manipulated as desired
pl.show()  # not necessary when executed in console
